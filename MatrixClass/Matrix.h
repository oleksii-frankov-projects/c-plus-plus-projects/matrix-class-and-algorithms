#pragma once
#include <iostream>

using namespace std;

class Matrix {
private:
	int rows, cols;
	float* pMatrix;
public:
	Matrix(int Rows, int Cols) {
		rows = Rows;
		cols = Cols;

		pMatrix = new float[rows * cols];
	}
	Matrix(int Size) {
		rows = Size;
		cols = Size;

		pMatrix = new float[rows * cols];
	}
	Matrix(const Matrix& matrix)
	{
		rows = matrix.rows;
		cols = matrix.cols;

		pMatrix = new float[rows * cols];

		for (int i = 0; i < rows * cols; i++) {

			pMatrix[i] = matrix.pMatrix[i];

		}
	}
	
	~Matrix() {
		delete[] pMatrix;
	}


	Matrix& operator = (const Matrix& matrix)
	{
		this->~Matrix();
		rows = matrix.rows;
		cols = matrix.cols;

		pMatrix = new float[rows * cols];

		for (int i = 0; i < rows * cols; i++) {
			pMatrix[i] = matrix.pMatrix[i];
		}
		return *this;
	}

	Matrix operator + (const Matrix& matrix) {
		if (rows == matrix.rows && cols == matrix.cols) {
			Matrix result(rows, cols);
			for (int i = 0; i < rows * cols; i++) {
				result.pMatrix[i] = pMatrix[i] + matrix.pMatrix[i];
			}
			return result;
		}
		else {
			cout << "Sum of different matrixes!" << endl;
			exit(1);
		}
	}

	Matrix operator - (const Matrix& matrix) {
		if (rows == matrix.rows && cols == matrix.cols) {
			Matrix result(rows, cols);
			for (int i = 0; i < rows * cols; i++) {
				result.pMatrix[i] = pMatrix[i] - matrix.pMatrix[i];
			}
			return result;
		}
		else {
			cout << "Difference of different matrixes!" << endl;
			exit(1);
		}

	}

	Matrix operator * (const Matrix& matrix) {
		if (cols == matrix.rows) {
			Matrix result(rows, matrix.cols);

			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < matrix.cols; j++) {
					result.pMatrix[i * result.cols + j] = 0;
					for (int k = 0; k < cols; k++) {
						result.pMatrix[i * result.cols + j] += pMatrix[i * cols + k] * matrix.pMatrix[k * matrix.cols + j];
					}
				}
			}
			return result;
		}
		else {
			cout << "Multiplication of inappropriate matrixes!" << endl;
			exit(1);
		}

	}

	Matrix operator * (float num) {
			Matrix result(rows, cols);
			for (int i = 0; i < rows * cols; i++) {
				result.pMatrix[i] = pMatrix[i] * num;
			}
			return result;
	}

	Matrix operator ~ (){
		Matrix result(cols, rows);
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				result.pMatrix[j * rows + i] = pMatrix[i * cols + j];
			}
		}
		return result;
	}

	float& operator () (int i, int j) {
		if (i < rows && j < cols) {
			return pMatrix[i * cols + j];
		}
		else {
			cout << "Wrong index of matrix element!" << endl;
			exit(1);
		}
	}

	float* operator[] (int i) {
		return pMatrix + i * cols;
	}


	bool operator == (const Matrix& matrix) {
		if (rows == matrix.rows && cols == matrix.cols) {
			for (int i = 0; i < rows * cols; i++) {
				if (pMatrix[i] != matrix.pMatrix[i]) {
					return false;
				}
			}
			return true;
		}
		else {
			return false;
		}

	}

	bool operator != (const Matrix& matrix) {
		return 1 - (*this == matrix);
	}

	friend istream& operator >> (istream& s, Matrix& matrix);
	friend ostream& operator << (ostream& s, const Matrix& matrix);
};

istream& operator >> (istream& s, Matrix& matrix) {

	for (int i = 0; i < matrix.rows * matrix.cols; i++) {
		s >> matrix.pMatrix[i];

	}
	return s;
}


ostream& operator<< (ostream& s, const Matrix& matrix) {
	for (int i = 0; i < matrix.rows; i++) {
		for (int j = 0; j < matrix.cols; j++) {
			s << matrix.pMatrix[i * matrix.cols + j] << " ";
		}
		s << endl;
	}
	return s;
}